package com.example.fastntasty.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Order {
    @SerializedName("id")
    private int id;
    @SerializedName("items")
    private List<OrderItem> items = new ArrayList<>();
    @SerializedName("time")
    private String time;
    @SerializedName("rating")
    private int rating;
    @SerializedName("comment")
    private String comment;
    @SerializedName("delivered")
    private boolean delivered;
    @SerializedName("anonymous")
    private boolean anonymous;
    @SerializedName("archived")
    private boolean archived;
    @SerializedName("customer")
    private Customer customer;

    public Order(int id, List<OrderItem> items, String time,
                 int rating, String comment, boolean delivered,
                 boolean anonymous, boolean archived, Customer customer) {
        this.id = id;
        this.items = items;
        this.time = time;
        this.rating = rating;
        this.comment = comment;
        this.delivered = delivered;
        this.anonymous = anonymous;
        this.archived = archived;
        this.customer = customer;
    }

    public Order(List<OrderItem> items, String time, int rating,
                 String comment, boolean delivered, boolean anonymous,
                 boolean archived, Customer customer) {
        this.items = items;
        this.time = time;
        this.rating = rating;
        this.comment = comment;
        this.delivered = delivered;
        this.anonymous = anonymous;
        this.archived = archived;
        this.customer = customer;
    }

    public Order(int id, int rating, String comment, boolean delivered, boolean anonymous) {
        this.id = id;
        this.rating = rating;
        this.comment = comment;
        this.delivered = delivered;
        this.anonymous = anonymous;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Order(boolean delivered, boolean anonymous, boolean archived, Customer customer) {
        this.delivered = delivered;
        this.anonymous = anonymous;
        this.archived = archived;
        this.customer = customer;
    }

    public Order() {
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", items=" + items +
                ", time='" + time + '\'' +
                ", rating=" + rating +
                ", comment='" + comment + '\'' +
                ", delivered=" + delivered +
                ", anonymous=" + anonymous +
                ", archived=" + archived +
                ", customer=" + customer +
                '}';
    }
}
