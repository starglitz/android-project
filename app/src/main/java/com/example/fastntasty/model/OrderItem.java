package com.example.fastntasty.model;

import com.google.gson.annotations.SerializedName;

public class OrderItem {

    @SerializedName("id")
    private Long id;
    @SerializedName("amount")
    private int amount;
    @SerializedName("article")
    private Article article;

    public OrderItem(int amount, Article article) {
        this.amount = amount;
        this.article = article;
    }

    public OrderItem(Long id, int amount, Article article) {
        this.id = id;
        this.amount = amount;
        this.article = article;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @Override
    public String toString() {
        return "Article: " + article + " , amount: " + amount;
    }


}
