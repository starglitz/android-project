package com.example.fastntasty.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SellersList {

    @SerializedName("sellers")
    private List<Seller> sellers;

    public SellersList( List<Seller> sellers) {

        this.sellers = sellers;
    }


    public List<Seller> getSellers() {
        return sellers;
    }

    public void setSellers(List<Seller> sellers) {
        this.sellers = sellers;
    }
}
