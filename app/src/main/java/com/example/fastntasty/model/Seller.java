package com.example.fastntasty.model;

import com.google.gson.annotations.SerializedName;

public class Seller {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("surname")
    private String surname;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("enabled")
    private boolean enabled;
    @SerializedName("address")
    private String address;
    @SerializedName("email")
    private String email;
    @SerializedName("sellerName")
    private String sellerName;
    @SerializedName("since")
    private String since;
    @SerializedName("rating")
    private float rating;

    public Seller(int id, String name, String surname, String username,
                  String password, boolean enabled, String address, String email,
                  String sellerName, String since) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.address = address;
        this.email = email;
        this.sellerName = sellerName;
        this.since = since;
    }

    public Seller(String name, String surname, String username,
                  String password, boolean enabled, String address,
                  String email, String sellerName) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.address = address;
        this.email = email;
        this.sellerName = sellerName;
    }

    public Seller(int id) {
        this.id = id;
    }

    public Seller(String address, String email, String sellerName, String since) {
        this.address = address;
        this.email = email;
        this.sellerName = sellerName;
        this.since = since;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
