package com.example.fastntasty.model;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;
import java.util.List;

public class Discount {

    @SerializedName("id")
    private Long id;
    @SerializedName("articles")
    private List<Article> articles;
    @SerializedName("percent")
    private int percent;
    @SerializedName("dateFrom")
    private LocalDate dateFrom;
    @SerializedName("dateTo")
    private LocalDate dateTo;
    @SerializedName("description")
    private String description;
    @SerializedName("seller")
    private Seller seller;

    public Discount(Long id, List<Article> articles, int percent,
                    LocalDate dateFrom, LocalDate dateTo, String description,
                    Seller seller) {
        this.id = id;
        this.articles = articles;
        this.percent = percent;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.description = description;
        this.seller = seller;
    }

    public Discount(List<Article> articles, int percent, LocalDate dateFrom, LocalDate dateTo, String description, Seller seller) {
        this.articles = articles;
        this.percent = percent;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.description = description;
        this.seller = seller;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }
}
