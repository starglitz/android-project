package com.example.fastntasty.model;

import com.google.gson.annotations.SerializedName;

public class Article {

    @SerializedName("id")
    private Long id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("price")
    private int price;
    @SerializedName("imgPath")
    private int imgPath;
    @SerializedName("path")
    private String path;
    @SerializedName("discounts")
    private int discounts;
    @SerializedName("seller")
    private Seller seller;

    public Article(String name, String description, int price, String path, int discounts, Seller seller) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.path = path;
        this.discounts = discounts;
        this.seller = seller;
    }

    public Article(String name, String description, int price, int imgPath) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imgPath = imgPath;
    }

    public Article(String name, String description, int price, String path, Seller seller) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.path = path;
        this.seller = seller;
    }

    public Article(Long id, String name, String description, int price, String path) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.path = path;
    }

    public Article() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getImgPath() {
        return imgPath;
    }

    public void setImgPath(int imgPath) {
        this.imgPath = imgPath;
    }

    public int getDiscounts() {
        return discounts;
    }

    public void setDiscounts(int discounts) {
        this.discounts = discounts;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    @Override
    public String toString() {
        return name;
    }

    public Article(Long id) {
        this.id = id;
    }
}
