package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.fastntasty.R;
import com.example.fastntasty.adapters.CommentAdapter;
import com.example.fastntasty.adapters.DiscountAdapter;

public class DiscountActivity extends AppCompatActivity {


    private ListView itemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);

        DiscountAdapter adapter = new DiscountAdapter(this);

        itemsList = findViewById(R.id.discountAdapt);
        itemsList.setAdapter(adapter);


    }

    public void openNewDiscount(View v) {
        Intent intent = new Intent(this, NewDiscountActivity.class);
        startActivity(intent);
    }
}