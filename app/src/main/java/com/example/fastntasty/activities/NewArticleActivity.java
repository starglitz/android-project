package com.example.fastntasty.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fastntasty.MainActivity;
import com.example.fastntasty.R;
import com.example.fastntasty.adapters.DrawerListAdapter;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.NavItem;
import com.example.fastntasty.model.Seller;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.ImageUploadService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewArticleActivity extends AppCompatActivity {

    private static int RESULT_LOAD_IMG = 1;
    //private static Context context;
    static final String TAG = NewArticleActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;
    private Article article = new Article();
    private String imageName;

    private SharedPreferences sharedPreferences;
    private ArticleService articleService;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mTitle;
    private ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();
    String encodedImage = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_article);

//        ImageView image_view = (ImageView) findViewById(R.id.img_view_upload);


        Button clickButton = (Button) findViewById(R.id.upload_img);
        clickButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);

            }
        });

        prepareMenu(mNavItems);

        mTitle = getTitle();
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mDrawerList = findViewById(R.id.navList);

        mDrawerPane = findViewById(R.id.drawerPane);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);

        // postavljamo senku koja preklama glavni sadrzaj
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // dodajemo listener koji ce reagovati na klik pojedinacnog elementa u listi
        mDrawerList.setOnItemClickListener(new NewArticleActivity.DrawerItemClickListener());
        // drawer-u postavljamo unapred definisan adapter
        mDrawerList.setAdapter(adapter);

        // Specificiramo da kada se drawer zatvori prikazujemo jednu ikonu
        // kada se drawer otvori drugu. Za to je potrebo da ispranvo povezemo
        // Toolbar i ActionBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Fast n' Tasty");
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setIcon(R.drawable.ic_launcher);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("iReviewer");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Izborom na neki element iz liste, pokrecemo akciju
        if (savedInstanceState == null) {
            selectItemFromDrawer(0);
        }
        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String userPref = sharedPreferences.getString("username", "");
        TextView userUsername = findViewById(R.id.userName);
        userUsername.setText(userPref);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    private void prepareMenu(ArrayList<NavItem> mNavItems ){
        mNavItems.add(new NavItem(getString(R.string.add_new_article), getString(R.string.add_new_article), R.drawable.ic_action_refresh));
        mNavItems.add(new NavItem(getString(R.string.home), getString(R.string.go_main), R.drawable.ic_action_refresh));
        mNavItems.add(new NavItem(getString(R.string.comments), getString(R.string.viewComments), R.drawable.ic_action_refresh));
        mNavItems.add(new NavItem(getString(R.string.discounts), getString(R.string.disc_detail), R.drawable.ic_action_refresh));
        mNavItems.add(new NavItem(getString(R.string.logout), getString(R.string.logout_fr), R.drawable.ic_action_refresh));


//        mNavItems.add(new NavItem(getString(R.string.about), getString(R.string.about_long), R.drawable.ic_action_about));
//        mNavItems.add(new NavItem(getString(R.string.sync_data), getString(R.string.sync_data_long), R.drawable.ic_action_refresh));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItemFromDrawer(position);
        }
    }

    private void selectItemFromDrawer(int position) {
        if(position == 0){


        }else if(position == 1){
            Intent intent = new Intent(this, MyArticlesActivity.class);
            startActivity(intent);
        }else if(position == 2){
            openComments();
        }else if(position == 3){
            openDiscounts();
            // FragmentTransition.to(new DiscountFragment(), this, false);
        }else if(position == 4){
            logout();
        }
        else{
            Log.e("DRAWER", "Nesto van opsega!");
        }

        mDrawerList.setItemChecked(position, true);
        if(position != 5) // za sve osim za sync
        {
            setTitle(mNavItems.get(position).getmTitle());
        }
        mDrawerLayout.closeDrawer(mDrawerPane);
    }



    public void openProfile(View v) {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    public void openComments() {
        Intent intent = new Intent(this, CommentActivity.class);
        startActivity(intent);
    }

    public void logout() {
        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void openDiscounts() {
        Intent intent = new Intent(this, DiscountActivity.class);
        startActivity(intent);
    }

    public void openNewArticle() {
        Intent intent = new Intent(this, NewArticleActivity.class);
        startActivity(intent);
    }

    public void openOrderReview() {
        Intent intent = new Intent(this, OrderReviewActivity.class);
        startActivity(intent);
    }




    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        ImageView image_view = (ImageView) findViewById(R.id.img_view_upload);


        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                image_view.setImageBitmap(selectedImage);


                if (selectedImage != null) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 20, byteArrayOutputStream);
                    byte[] selectedImageBytes = byteArrayOutputStream.toByteArray();
                    encodedImage = Base64.encodeToString(selectedImageBytes, Base64.DEFAULT);
                    article.setPath(encodedImage);
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(NewArticleActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }else {
            Toast.makeText(NewArticleActivity.this, "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }



    public void createArticle(View v) {
        System.out.println("VALUE OF IMAGE NAME HERE: ");
        System.out.println(imageName);
        System.out.println(article.getPath());

        //findViewById(R.id.img_view_upload);
        EditText name = findViewById(R.id.article_name);
        EditText description = findViewById(R.id.article_description);
        EditText price = findViewById(R.id.article_price);


        String nameVal = name.getText().toString();
        String descriptionVal = description.getText().toString();
        String priceVal = price.getText().toString();

        if(nameVal.isEmpty() || descriptionVal.isEmpty() || priceVal.isEmpty()) {
            Toast.makeText(NewArticleActivity.this, "Make sure to fill all fields!", Toast.LENGTH_LONG).show();
        }

        else if(article.getPath() == null || article.getPath() == "") {
            Toast.makeText(NewArticleActivity.this, "You haven't uploaded an image!", Toast.LENGTH_LONG).show();
        }

        else if(!isNumeric(priceVal)) {
            Toast.makeText(NewArticleActivity.this, "Price must be a number!", Toast.LENGTH_LONG).show();
        }

        else if(Integer.parseInt(priceVal) < 1) {
            Toast.makeText(NewArticleActivity.this, "Price has to be a positive number!", Toast.LENGTH_LONG).show();
        }

        else if(Integer.parseInt(priceVal) > 999999) {
            Toast.makeText(NewArticleActivity.this, "Be realistic", Toast.LENGTH_LONG).show();
        }

        else if(encodedImage == "") {
            Toast.makeText(NewArticleActivity.this, "You haven't picked an image", Toast.LENGTH_LONG).show();
        }

        else {

            sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
            Long sellerId = sharedPreferences.getLong("id", 0L);

            Seller seller = new Seller(sellerId.intValue());

            //String name, String description, String price, String path, Seller seller
            Article newArticle = new Article(nameVal, descriptionVal, Integer.parseInt(priceVal), article.getPath(), seller);
            //newArticle.setPath(encodedImage);

            if (retrofit == null) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
            }

            articleService = retrofit.create(ArticleService.class);
            Call<Article> call = articleService.addArticle(newArticle);

            call.enqueue(new Callback<Article>() {
                @Override
                public void onResponse(Call<Article> call, Response<Article> response) {
                   // recyclerView.setAdapter(new MyArticlesAdapter(response.body(), MyArticlesActivity.this));
                    System.out.println("SUCCESS");
                    System.out.println(response.body());
                    Intent intent = new Intent(NewArticleActivity.this, MyArticlesActivity.class);
                    startActivity(intent);
                    Toast.makeText(NewArticleActivity.this, "Successfully added article",Toast.LENGTH_LONG).show();

                    // recyclerView.setAdapter(new MovieListAdapter(response.body().getMovies(), getApplicationContext()));
                }
                @Override
                public void onFailure(Call<Article> call, Throwable throwable) {
                    Log.e(TAG, throwable.toString());
                    System.out.println("hereee");
                }
            });

        }
    }




}