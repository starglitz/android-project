package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.media.Image;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fastntasty.R;
import com.example.fastntasty.adapters.OrderItemAdapter;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.model.OrderItem;
import com.example.fastntasty.services.ArticleService;
import com.google.gson.Gson;
import com.squareup.seismic.ShakeDetector;

import java.io.IOException;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ArticleActivity extends AppCompatActivity implements ShakeDetector.Listener {

    private SharedPreferences sharedPreferences;

    static final String TAG = ArticleActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;
    EditText amount;
    private static final int SHAKE_THRESHOLD = 800;
    private Article article;

    TextView name;
    TextView price;
    TextView description;
    ImageView img;


    @Override public void hearShake() {
        if(amount.getText().toString().equals("")) {
            amount.setText("1");
        }
        else {
            amount.setText(String.valueOf(Integer.parseInt(amount.getText().toString()) + 1));
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector sd = new ShakeDetector(this);
        sd.start(sensorManager);



        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        price = findViewById(R.id.price);
        img = findViewById(R.id.imageArticle);
        amount = findViewById(R.id.amount);

        fetchArticle(getIntent().getLongExtra("ID", 0L));


        Button addtocart = findViewById(R.id.addtocart);
        addtocart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                System.out.println("TEXT " + amount.getText());
                System.out.println("TEXT " + amount.getText().toString());

                if(amount.getText() == null) {
                    Toast.makeText(ArticleActivity.this, "You must choose a number!" , Toast.LENGTH_LONG).show();
                }

                else if(amount.getText().toString().equals("")) {
                    Toast.makeText(ArticleActivity.this, "You must choose a number!" , Toast.LENGTH_LONG).show();
                }

                else if(Integer.parseInt(amount.getText().toString()) < 1) {
                    Toast.makeText(ArticleActivity.this, "Amount must be a positive number" , Toast.LENGTH_LONG).show();
                }

                else {
                Integer amountInt = Integer.parseInt(String.valueOf(amount.getText()));
                Long articleID = getIntent().getLongExtra("ID", 0L);
                System.out.println("ARTICLE ID: " + articleID);


                if (retrofit == null) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }
                ArticleService articleService = retrofit.create(ArticleService.class);
                Call<Article> call = articleService.getArticle(articleID);

                call.enqueue(new Callback<Article>() {
                    @Override
                    public void onResponse(Call<Article> call, Response<Article> response) {
                        OrderItem item = new OrderItem(amountInt, response.body());
                        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

                        Gson gson = new Gson();
                        String order = sharedPreferences.getString("order", "");

                        Order orderObj = gson.fromJson(order, Order.class);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        orderObj.getItems().add(item);

                        editor.putString("order", gson.toJson(orderObj));
                        editor.commit();
                        System.out.println(orderObj);

                        Intent intent = new Intent(getApplicationContext(), SellersArticlesActivity.class);

                        Bundle b = new Bundle();
                        b.putInt("SellerID", Integer.parseInt(String.valueOf(getIntent().getLongExtra("SellerID", 0L))));
                        intent.putExtras(b);

                        startActivity(intent);

                    }
                    @Override
                    public void onFailure(Call<Article> call, Throwable throwable) {
                        Log.e(TAG, throwable.toString());
                        System.out.println("hereee");
                    }
                });

            }
            }
        });


    }


    public void fetchArticle(Long articleID) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }



        ArticleService articleService = retrofit.create(ArticleService.class);
        Call<Article> call = articleService.getArticle(articleID);
        //Call<List<Seller>> call = sellerService.getSellers();
        System.out.println("!!!");
        System.out.println(call);
        call.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Call<Article> call, Response<Article> response) {
                // recyclerView.setAdapter(new MyArticlesAdapter(response.body(), MyArticlesActivity.this));
                System.out.println("SUCCESS");
                System.out.println(response.body());
                Article a = response.body();
                name.setText(a.getName());
                description.setText(a.getDescription());
                price.setText(String.valueOf(a.getPrice()));

                byte[] bytes = Base64.decode(a.getPath(), Base64.DEFAULT);
                Bitmap bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                img.setImageBitmap(bm);
                // recyclerView.setAdapter(new MovieListAdapter(response.body().getMovies(), getApplicationContext()));
            }
            @Override
            public void onFailure(Call<Article> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
                System.out.println("hereee");
            }
        });
    }


}