package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.fastntasty.MainActivity;
import com.example.fastntasty.R;
import com.example.fastntasty.adapters.MyArticlesAdapter;
import com.example.fastntasty.adapters.UsersAdapter;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.User;
import com.example.fastntasty.model.UserBlock;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.UserService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UsersActivity extends AppCompatActivity {

    static final String TAG = UsersActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;
    private RecyclerView recyclerView;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        recyclerView = findViewById(R.id.usersBlockAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onResume(){
        super.onResume();
        getUsers();
    }



    private void getUsers() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        UserService userService = retrofit.create(UserService.class);
        Call<List<UserBlock>> call = userService.getAllNoAdmin();
        System.out.println("!!!");
        System.out.println(call);
        call.enqueue(new Callback<List<UserBlock>>() {
            @Override
            public void onResponse(Call<List<UserBlock>> call, Response<List<UserBlock>> response) {
                //recyclerView.setAdapter(new MyArticlesAdapter(response.body(), MyArticlesActivity.this));
                System.out.println("DID I GO HERE");
                recyclerView.setAdapter(new UsersAdapter(response.body(), UsersActivity.this));
                System.out.println("SUCCESS");
                System.out.println(response.body());
                // recyclerView.setAdapter(new MovieListAdapter(response.body().getMovies(), getApplicationContext()));
            }
            @Override
            public void onFailure(Call<List<UserBlock>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
                System.out.println("hereee");
            }
        });
    }

    public void logout(View v) {
        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }



}