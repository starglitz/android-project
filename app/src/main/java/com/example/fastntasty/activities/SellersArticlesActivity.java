package com.example.fastntasty.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fastntasty.MainActivity;
import com.example.fastntasty.R;
import com.example.fastntasty.adapters.ArticleListAdapter;
import com.example.fastntasty.adapters.CommentsCustomerAdapter;
import com.example.fastntasty.adapters.DrawerListAdapter;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Customer;
import com.example.fastntasty.model.NavItem;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.OrderService;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SellersArticlesActivity extends AppCompatActivity {

    int id;
    float rating;
    static final String TAG = SellersArticlesActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;
    private RecyclerView recyclerView;
    private RecyclerView recyclerView2;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private RelativeLayout mDrawerPane;
    private CharSequence mTitle;
    private ArrayList<NavItem> mNavItems = new ArrayList<NavItem>();


    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sellers_articles);


//boolean delivered, boolean anonymous, boolean archived, Customer customer
        Bundle b = getIntent().getExtras();
        if(b != null) {
            id = b.getInt("SellerID");
            rating = b.getFloat("SellerRating");
        }

        recyclerView = findViewById(R.id.articlesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView2 = findViewById(R.id.commentsAdapterCustomer);
        recyclerView2.setLayoutManager(new LinearLayoutManager(this));

        Gson gson = new Gson();
        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String order = sharedPreferences.getString("order", "");

        Long userID = sharedPreferences.getLong("id", 0L);
        Order orderObj = new Order();

        if(order == "") {
            System.out.println("INITIAL ORDER:");
            Order initial = new Order(false, false, false, new Customer(Integer.parseInt(String.valueOf(userID))));

            System.out.println(initial);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("order", gson.toJson(initial));
            editor.commit();
        }
        else {
            orderObj = gson.fromJson(order, Order.class);
            System.out.println("ORDER IN SELLERS ARTICLES, UPDATED: ");
            System.out.println(orderObj);
        }

        Button finish = findViewById(R.id.finish_order);
        Order finalOrderObj = orderObj;
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(finalOrderObj.getItems().size() == 0) {
                    Toast.makeText(SellersArticlesActivity.this, "You haven't picked any articles!" , Toast.LENGTH_LONG).show();
                }
                else {
                    Intent intent = new Intent(SellersArticlesActivity.this, FinishOrderActivity.class);
                    startActivity(intent);
                }
//                Intent intent = new Intent(context,SellersArticlesActivity.class);
//                Bundle b = new Bundle();
//                b.putInt("SellerID", seller.getId());
//                intent.putExtras(b);
//                context.startActivity(intent);
            }
        });

        prepareMenu(mNavItems);

        mTitle = getTitle();
        mDrawerLayout = findViewById(R.id.drawerLayout);
        mDrawerList = findViewById(R.id.navList);

        mDrawerPane = findViewById(R.id.drawerPane);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);

        // postavljamo senku koja preklama glavni sadrzaj
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // dodajemo listener koji ce reagovati na klik pojedinacnog elementa u listi
        mDrawerList.setOnItemClickListener(new SellersArticlesActivity.DrawerItemClickListener());
        // drawer-u postavljamo unapred definisan adapter
        mDrawerList.setAdapter(adapter);

        // Specificiramo da kada se drawer zatvori prikazujemo jednu ikonu
        // kada se drawer otvori drugu. Za to je potrebo da ispranvo povezemo
        // Toolbar i ActionBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Fast n' Tasty");
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setIcon(R.drawable.ic_launcher);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_drawer);
            actionBar.setHomeButtonEnabled(true);
        }

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("iReviewer");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Izborom na neki element iz liste, pokrecemo akciju
        if (savedInstanceState == null) {
            selectItemFromDrawer(0);
        }

        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String userPref = sharedPreferences.getString("username", "");
        TextView userUsername = findViewById(R.id.userName);
        userUsername.setText(userPref);
    }

    @Override
    public void onResume(){
        super.onResume();
        getSellersArticles();
        getComments();
    }

    private void prepareMenu(ArrayList<NavItem> mNavItems ){
        mNavItems.add(new NavItem(getString(R.string.stay_on_page), getString(R.string.stay_on_page), R.drawable.ic_action_refresh));
        mNavItems.add(new NavItem(getString(R.string.go_home), getString(R.string.go_to_home),R.drawable.ic_action_refresh));
        mNavItems.add(new NavItem(getString(R.string.order_review), getString(R.string.order_review_detail), R.drawable.ic_action_refresh));
        mNavItems.add(new NavItem(getString(R.string.logout), getString(R.string.logout_fr), R.drawable.ic_action_refresh));
//        mNavItems.add(new NavItem(getString(R.string.about), getString(R.string.about_long), R.drawable.ic_action_about));
//        mNavItems.add(new NavItem(getString(R.string.sync_data), getString(R.string.sync_data_long), R.drawable.ic_action_refresh));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItemFromDrawer(position);
        }
    }

    private void selectItemFromDrawer(int position) {
        if(position == 0){

        }else if(position == 1){
            Intent intent = new Intent(this, SellersActivity.class);
            startActivity(intent);
        }else if(position == 2){
            openOrderReview();
        }else if(position == 3){
           logout();
            // FragmentTransition.to(new DiscountFragment(), this, false);
        }
        else{
            Log.e("DRAWER", "Nesto van opsega!");
        }

        mDrawerList.setItemChecked(position, true);
        if(position != 5) // za sve osim za sync
        {
            setTitle(mNavItems.get(position).getmTitle());
        }
        mDrawerLayout.closeDrawer(mDrawerPane);
    }



    public void openProfile(View v) {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    public void openComments() {
        Intent intent = new Intent(this, CommentActivity.class);
        startActivity(intent);
    }

    public void logout() {

        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void openDiscounts() {
        Intent intent = new Intent(this, DiscountActivity.class);
        startActivity(intent);
    }

    public void openNewArticle() {
        Intent intent = new Intent(this, NewArticleActivity.class);
        startActivity(intent);
    }

    public void openOrderReview() {
        Intent intent = new Intent(this, OrderReviewActivity.class);
        startActivity(intent);
    }

    private void getSellersArticles() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        ArticleService articleService = retrofit.create(ArticleService.class);
        Call<List<Article>> call = articleService.getSellersArticles((long) id);
        //Call<List<Seller>> call = sellerService.getSellers();
        System.out.println("!!!");
        System.out.println(call);
        call.enqueue(new Callback<List<Article>>() {
            @Override
            public void onResponse(Call<List<Article>> call, Response<List<Article>> response) {
                recyclerView.setAdapter(new ArticleListAdapter(response.body(), SellersArticlesActivity.this));
                System.out.println("SUCCESS");
                System.out.println(response.body());
                if(response.body().size() == 0) {
                    TextView noArticles = findViewById(R.id.noArticles);
                    noArticles.setText("This seller has no articles for sale");
                }
                // recyclerView.setAdapter(new MovieListAdapter(response.body().getMovies(), getApplicationContext()));
            }
            @Override
            public void onFailure(Call<List<Article>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
                System.out.println("hereee");
            }
        });
    }

    private void getComments() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }


        OrderService orderService = retrofit.create(OrderService.class);

        Call<List<Order>> call = orderService.getBySeller(Long.parseLong(String.valueOf(id)));

        System.out.println("!!!");
        System.out.println(call);
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                List<Order> orders= response.body();
                List<Order> commented = new ArrayList<>();
                for(Order order : orders) {
                    if(order.getRating() != 0 && !order.isArchived()) {
                        commented.add(order);
                    }
                }
                recyclerView2.setAdapter(new CommentsCustomerAdapter(commented, SellersArticlesActivity.this));
                System.out.println("SUCCESS");
                System.out.println(response.body());


                if(commented.size() != 0) {
                    TextView customerReviews = findViewById(R.id.customerReviews);
                    customerReviews.setText("Customer reviews:");
                    TextView averageRating = findViewById(R.id.averageRating);
                    averageRating.setText("average rating: " + String.valueOf(rating));
                }

            }
            @Override
            public void onFailure(Call<List<Order>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
                System.out.println("hereee");
            }
        });
    }

}