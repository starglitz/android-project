package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import com.example.fastntasty.R;

public class ProfileActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

        String userPref = sharedPreferences.getString("username", "");

        TextView username = findViewById(R.id.currentUsername);
        username.setText(userPref);
    }


}