package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fastntasty.MainActivity;
import com.example.fastntasty.R;
import com.example.fastntasty.model.Customer;
import com.example.fastntasty.model.Seller;
import com.example.fastntasty.services.CustomerService;
import com.example.fastntasty.services.SellerService;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterSellerActivity extends AppCompatActivity {

    static final String TAG = RegisterSellerActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    private SellerService sellerService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_seller);
    }

    public void registerClick(View v)
    {
        EditText name = findViewById(R.id.register_name);
        EditText surname = findViewById(R.id.register_surname);
        EditText username = findViewById(R.id.register_username);
        EditText password = findViewById(R.id.register_password);
        EditText address = findViewById(R.id.register_address);
        EditText email = findViewById(R.id.register_email);
        EditText sellerName = findViewById(R.id.register_sellerName);

        String nameVal = name.getText().toString();
        String surnameVal = surname.getText().toString();
        String usernameVal = username.getText().toString();
        String passwordVal = password.getText().toString();
        String addressVal = address.getText().toString();
        String emailVal = email.getText().toString();
        String sellerNameVal = sellerName.getText().toString();


        //public Customer(int id, String name, String surname, String username, String password, boolean enabled, String address) {

       // Customer customer = new Customer(nameVal, surnameVal, usernameVal, passwordVal, true, addressVal);

//        String name, String surname, String username,
//            String password, boolean enabled, String address,
//            String email, String sellerName)

        Seller seller = new Seller(nameVal, surnameVal, usernameVal, passwordVal, true, addressVal, emailVal, sellerNameVal);

        if(validate(nameVal, surnameVal, usernameVal, passwordVal, addressVal, emailVal, sellerNameVal)) {

            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }

            sellerService = retrofit.create(SellerService.class);
            //CustomerService customerService = retrofit.create(CustomerService.class);
            //customerService.register(customer);


            //Call<Result> call = postService.addTagInPost(postBody.getId(), tagBody.getId());
            Call<Seller> call = sellerService.register(seller);
            //Call<Customer> call = sellerService.register(customer);
            call.enqueue(new Callback<Seller>() {
                @Override
                public void onResponse(Call<Seller> call, Response<Seller> response) {
                    Toast.makeText(RegisterSellerActivity.this, "successfully registered! Log in before you start using the application" , Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(RegisterSellerActivity.this, MainActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onFailure(Call<Seller> call, Throwable t) {
                    Toast.makeText(RegisterSellerActivity.this, "Make sure data you entered is valid!" , Toast.LENGTH_LONG).show();
                }
            });
            //Call<List<Article>> call = articleService.getSellersArticles((long) id);
            //Toast.makeText(this, "Clicked on Button", Toast.LENGTH_LONG).show();
        }

    }

    public boolean validate(String name, String surname, String username, String password,
                            String address, String email, String sellerName) {

        boolean ok = true;

        if(name.isEmpty() || surname.isEmpty() || username.isEmpty() || password.isEmpty()
                || address.isEmpty() || email.isEmpty() || sellerName.isEmpty()) {
            Toast.makeText(this, "Make sure to fill all fields!" , Toast.LENGTH_LONG).show();
            ok = false;
        }

        else if(!isValidEmail(email)) {
            Toast.makeText(this, "Email is in invalid format!" , Toast.LENGTH_LONG).show();
            ok = false;
        }

        return ok;
    }
    public static boolean isValidEmail(String email) {
        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}