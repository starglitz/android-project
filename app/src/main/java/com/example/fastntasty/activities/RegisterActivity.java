package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fastntasty.MainActivity;
import com.example.fastntasty.R;
import com.example.fastntasty.fragments.RegisterFragment;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Customer;
import com.example.fastntasty.model.Seller;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.CustomerService;
import com.example.fastntasty.services.SellerService;
import com.example.fastntasty.tools.FragmentTransition;

import java.util.List;

import javax.xml.transform.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    static final String TAG = RegisterActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    private CustomerService customerService;
    private SellerService sellerService;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

//        Spinner spinner = (Spinner) findViewById(R.id.roles_spinner);
//// Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.roles_array, android.R.layout.simple_spinner_item);
//// Specify the layout to use when the list of choices appears
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//// Apply the adapter to the spinner
//        spinner.setAdapter(adapter);
//
//
//
//
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                String text = spinner.getSelectedItem().toString();
//                Context context = getApplicationContext();
////                Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
//
////                FragmentManager fragmentManager = getFragmentManager();
////                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//
////                Fragment regPart = new RegisterFragment();
////                fragmentTransaction.add(R.id.reg_fragment, regPart);
////                fragmentTransaction.commit();
//
//
////                FragmentTransition.to(new RegisterFragment());
//
////                getSupportFragmentManager().beginTransaction()
////                        .setReorderingAllowed(true)
////                        .add(R.id.reg_fragment, RegisterFragment.class, null)
////                        .commit();
//
//            }
//
//            public void onNothingSelected(AdapterView<?> adapterView) {
//                return;
//            }
//        });




    }

    public void registerClick(View v)
    {
        EditText name = findViewById(R.id.register_name);
        EditText surname = findViewById(R.id.register_surname);
        EditText username = findViewById(R.id.register_username);
        EditText password = findViewById(R.id.register_password);
        EditText address = findViewById(R.id.register_address);
        //Spinner role = findViewById(R.id.roles_spinner);

        String nameVal = name.getText().toString();
        String surnameVal = surname.getText().toString();
        String usernameVal = username.getText().toString();
        String passwordVal = password.getText().toString();
        String addressVal = address.getText().toString();


        //public Customer(int id, String name, String surname, String username, String password, boolean enabled, String address) {

        Customer customer = new Customer(nameVal, surnameVal, usernameVal, passwordVal, true, addressVal);
        //Toast.makeText(this, customer.toString(), Toast.LENGTH_LONG).show();


        if(validate(nameVal, surnameVal, usernameVal, passwordVal, addressVal)) {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        customerService = retrofit.create(CustomerService.class);
        //CustomerService customerService = retrofit.create(CustomerService.class);
        //customerService.register(customer);


        //Call<Result> call = postService.addTagInPost(postBody.getId(), tagBody.getId());
        Call<Customer> call = customerService.register(customer);
        call.enqueue(new Callback<Customer>() {
            @Override
            public void onResponse(Call<Customer> call, Response<Customer> response) {
                Toast.makeText(RegisterActivity.this, "successfully registered! Log in before you start using the application" , Toast.LENGTH_LONG).show();
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<Customer> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Make sure data you entered is valid!" , Toast.LENGTH_LONG).show();
            }
        });
        //Call<List<Article>> call = articleService.getSellersArticles((long) id);
        //Toast.makeText(this, "Clicked on Button", Toast.LENGTH_LONG).show();
        }

    }

    public boolean validate(String name, String surname, String username, String password, String address) {

        boolean ok = true;

        if(name.isEmpty() || surname.isEmpty() || username.isEmpty() || password.isEmpty() || address.isEmpty()) {
            Toast.makeText(this, "Make sure to fill all fields!" , Toast.LENGTH_LONG).show();
            ok = false;
        }
        return ok;
    }
}