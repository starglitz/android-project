package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.ListView;

import com.example.fastntasty.R;
import com.example.fastntasty.adapters.ArticleAdapter;
import com.example.fastntasty.adapters.ArticleCartAdapter;

public class CartActivity extends AppCompatActivity {

    private ListView itemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);



        toolbar.setTitle("Finish your order");

        ArticleCartAdapter adapter = new ArticleCartAdapter(this);

        itemsList = findViewById(R.id.orderItemsAdapt);
        itemsList.setAdapter(adapter);

    }
}