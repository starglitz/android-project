package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fastntasty.R;
import com.example.fastntasty.adapters.ArticleListAdapter;
import com.example.fastntasty.adapters.OrderItemAdapter;
import com.example.fastntasty.adapters.SellersListAdapter;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Customer;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.model.OrderItem;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.OrderService;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FinishOrderActivity extends AppCompatActivity {


    SharedPreferences sharedPreferences;
    TextView totalPrice;

    static final String TAG = FinishOrderActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_order);

        RecyclerView recyclerView = findViewById(R.id.orderItemAdapter2);
        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String order = sharedPreferences.getString("order", "");
        Gson gson = new Gson();
        Order orderObj = gson.fromJson(order, Order.class);
        System.out.println("ORDER IN FINISH ORDER: ");
        System.out.println(orderObj);

        if(orderObj.getItems().size() == 0) {
            TextView noItems = findViewById(R.id.noItems);
            noItems.setText("You haven't picked any items!");
        }

        totalPrice = findViewById(R.id.price_total);

        int total = 0;
        for(OrderItem item : orderObj.getItems()) {
            System.out.println("PRICE 1:" + total);
            System.out.println(item.getAmount() * item.getArticle().getPrice());
            total += item.getAmount() * item.getArticle().getPrice();
        }
        System.out.println("TOTAL PRICE:" + total);
        totalPrice.setText("Total price: " + String.valueOf(total));

        System.out.println("IS THIS EXECUTING");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new OrderItemAdapter(orderObj.getItems(), FinishOrderActivity.this));

        //totalPrice.setText("Test");

        Button buy = findViewById(R.id.buy_items);
        buy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }

                OrderService orderService = retrofit.create(OrderService.class);
                Call<Order> call = orderService.add(orderObj);
                call.enqueue(new Callback<Order>() {
                    @Override
                    public void onResponse(Call<Order> call, Response<Order> response) {
                        Gson gson = new Gson();
                        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
                        String order = sharedPreferences.getString("order", "");
                        System.out.println("yay");
                        Long userID = sharedPreferences.getLong("id", 0L);

                        Order initial = new Order(false, false, false, new Customer(Integer.parseInt(String.valueOf(userID))));

                        System.out.println(initial);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("order", gson.toJson(initial));
                        editor.commit();
                        Toast.makeText(FinishOrderActivity.this, "Successfully bought!" , Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(FinishOrderActivity.this, SellersActivity.class);
                        startActivity(intent);
                        // recyclerView.setAdapter(new MovieListAdapter(response.body().getMovies(), getApplicationContext()));
                    }
                    @Override
                    public void onFailure(Call<Order> call, Throwable throwable) {
                        Log.e(TAG, throwable.toString());
                        System.out.println("boo :(");
                    }
                });
            }


        });
    }
}