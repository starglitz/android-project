package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fastntasty.R;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.model.OrderItem;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.OrderService;
import com.google.gson.Gson;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddCommentActivity extends AppCompatActivity {

    Long id;
    EditText orderComment;
    EditText orderRating;
    CheckBox isAnonymous;
    Button rateOrder;
    boolean anonymous;

    static final String TAG = AddCommentActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_comment);

        TextView idComment = findViewById(R.id.id_from_add_comment);

        Bundle b = getIntent().getExtras();
        if(b != null) {
            id = b.getLong("ID", 0L);
        }

        orderComment = findViewById(R.id.orderComment);
        orderRating = findViewById(R.id.orderRating);
        isAnonymous = findViewById(R.id.isAnonymous);
        rateOrder = findViewById(R.id.rateOrder);

        idComment.setText(String.valueOf(id));


        rateOrder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String comment = orderComment.getText().toString();
                String rating = orderRating.getText().toString();
             //   int ratingInt = Integer.parseInt(rating);

                if(orderComment.getText().toString().equals("") || orderRating.getText().toString().equals("")) {
                    Toast.makeText(AddCommentActivity.this, "You have to fill all fields!" , Toast.LENGTH_LONG).show();
                    System.out.println("IM HERE!!!!!!!");
                }
                else if(orderComment.getText() == null || orderRating.getText() == null) {
                    Toast.makeText(AddCommentActivity.this, "You have to fill all fields!" , Toast.LENGTH_LONG).show();
                    System.out.println("IM HERE!!!!!!!");
                }
                else{

                    if (Integer.parseInt(orderRating.getText().toString()) < 1 || Integer.parseInt(rating) > 5) {
                        Toast.makeText(AddCommentActivity.this, "Rating has to be between 1 and 5!", Toast.LENGTH_LONG).show();
                    } else {
                        Integer ratingInt = Integer.parseInt(rating);
                        if (isAnonymous.isChecked()) {
                            anonymous = true;
                        } else {
                            anonymous = false;
                        }

                        if (retrofit == null) {
                            retrofit = new Retrofit.Builder()
                                    .baseUrl(BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                        }
                        OrderService orderService = retrofit.create(OrderService.class);
                        //int id, int rating, String comment, boolean delivered, boolean anonymous
                        Order order = new Order(Integer.parseInt(String.valueOf(id)), ratingInt, comment, true, anonymous);
                        order.setArchived(false);
                        Call<Order> call = orderService.update(id, order);

                        call.enqueue(new Callback<Order>() {
                            @Override
                            public void onResponse(Call<Order> call, Response<Order> response) {
                                Toast.makeText(AddCommentActivity.this, "Successfully rated an order!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), OrderReviewActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onFailure(Call<Order> call, Throwable throwable) {
                                Log.e(TAG, throwable.toString());
                                System.out.println("hereee");
                            }
                        });
                    }
                }
            }
        });

    }


}