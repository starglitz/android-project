package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.fastntasty.R;

public class ToolbarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);
    }
}