package com.example.fastntasty.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.fastntasty.R;
import com.example.fastntasty.adapters.ArticleCartAdapter;
import com.example.fastntasty.adapters.CommentAdapter;
import com.example.fastntasty.adapters.CommentsAdapter;
import com.example.fastntasty.adapters.MyArticlesAdapter;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.OrderService;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CommentActivity extends AppCompatActivity {


   // private ListView itemsList;
    static final String TAG = CommentActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;
    private RecyclerView recyclerView;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.commentsAdapt);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

     //   toolbar.setTitle("Comments");
//
//        CommentAdapter adapter = new CommentAdapter(this);
//
//        itemsList = findViewById(R.id.commentsAdapt);
//        itemsList.setAdapter(adapter);
    }
    @Override
    public void onResume(){
        super.onResume();
        getComments();
    }

    private void getComments() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

        String userPref = sharedPreferences.getString("username", "");
        Long id = sharedPreferences.getLong("id", 0L);

        OrderService orderService = retrofit.create(OrderService.class);
       // ArticleService articleService = retrofit.create(ArticleService.class);
        Call<List<Order>> call = orderService.getBySeller(id);
        //Call<List<Article>> call = articleService.getSellersArticles(id);
        //Call<List<Seller>> call = sellerService.getSellers();
        System.out.println("!!!");
        System.out.println(call);
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                List<Order> orders= response.body();
                List<Order> commented = new ArrayList<>();
                for(Order order : orders) {
                    if(order.getRating() != 0 && !order.isArchived()) {
                        commented.add(order);
                    }
                }
                recyclerView.setAdapter(new CommentsAdapter(commented, CommentActivity.this));
                System.out.println("SUCCESS");
                System.out.println(response.body());
                // recyclerView.setAdapter(new MovieListAdapter(response.body().getMovies(), getApplicationContext()));
            }
            @Override
            public void onFailure(Call<List<Order>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
                System.out.println("hereee");
            }
        });
    }
}