package com.example.fastntasty.services;

import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Customer;
import com.example.fastntasty.model.Seller;
import com.example.fastntasty.model.SellersList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface SellerService {
    @GET("sellers")
    Call<List<Seller>> getSellers();

    @POST("registerSeller")
    Call<Seller> register(@Body Seller seller);
}
