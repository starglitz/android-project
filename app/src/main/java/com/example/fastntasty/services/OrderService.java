package com.example.fastntasty.services;

import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Order;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface OrderService {
    @POST("order")
    Call<Order> add(@Body Order order);

    @GET("order/customer/{id}")
    Call<List<Order>> getByCustomer(@Path("id") Long id);

    @PUT("order/{id}")
    Call<Order> update(@Path("id") Long id, @Body Order order);

    @GET("order/seller/{id}")
    Call<List<Order>> getBySeller(@Path("id") Long id);

    @PUT("order/archived/{id}")
    Call<Order> setArchived(@Path("id") Long id, @Body Order order);
}
