package com.example.fastntasty.services;

import android.icu.lang.UScript;

import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Seller;
import com.example.fastntasty.model.User;
import com.example.fastntasty.model.UserBlock;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @POST("login")
    Call<User> login(@Body User user);

    @PUT("users/block/{id}")
    Call<UserBlock> block(@Path("id") Long id, @Body UserBlock userBlock);

    @GET("users")
    Call<List<UserBlock>> getAll();

    @GET("users/noAdmin")
    Call<List<UserBlock>> getAllNoAdmin();

    @GET("users/{username}")
    Call<User> findByUsername(@Path("username") String username);

}
