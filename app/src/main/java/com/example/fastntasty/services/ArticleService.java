package com.example.fastntasty.services;

import com.example.fastntasty.model.Article;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("articles/{id}")
    Call<Article> getArticle(@Path("id") Long id);

    @GET("articles")
    Call<List<Article>> getArticles();

    @POST("articles")
    Call<Article> addArticle(@Body Article article);

    @GET("articles/seller/{id}")
    Call<List<Article>> getSellersArticles(@Path("id") Long id);


    @DELETE("articles/{id}")
    Call<Article> deleteArticle(@Path("id") Long id);

    @PUT("articles/{id}")
    Call<Article> updateArticle(@Path("id") Long id, @Body Article article);
}
