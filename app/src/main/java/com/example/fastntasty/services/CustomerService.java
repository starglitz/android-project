package com.example.fastntasty.services;

import com.example.fastntasty.model.Customer;
import com.example.fastntasty.model.Seller;

import java.util.List;

import javax.xml.transform.Result;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface CustomerService {


//    @FormUrlEncoded
    @POST("registerCustomer")
    Call<Customer> register(@Body Customer customer);
}
