package com.example.fastntasty.tools;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.fastntasty.R;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Customer;
import com.example.fastntasty.model.Discount;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.model.OrderItem;
import com.example.fastntasty.model.Seller;

public class MockupData {

    public static List<Seller> getSellers() {

        ArrayList<Seller> sellers = new ArrayList<>();
        Seller seller1 = new Seller("Marka Miljanova 10", "seller1@gmail.com", "Seller 1", "Providing since 1970.");
        Seller seller2 = new Seller("Bulevar Evrope 168", "seller2@gmail.com", "Seller 2", "Providing since 2000.");
        Seller seller3 = new Seller("Bulevar Oslobodjenja 5", "seller3@gmail.com", "Seller 3", "Providing since 2010.");


        sellers.add(seller1);
        sellers.add(seller2);
        sellers.add(seller3);

        return sellers;
    }

    public static List<Article> getArticles() {

        ArrayList<Article> articles = new ArrayList<>();
//        Article article1 = new Article("Burger", "Best of our own", "300rsd", R.drawable.food1);
//        Article article2 = new Article("Pancake", "Best in town", "150rsd", R.drawable.pancake);
//        Article article3 = new Article("Omelette", "Basic breakfast", "200rsd", R.drawable.omelette);
//
//        articles.add(article1);
//        articles.add(article2);
//        articles.add(article3);

        return articles;
    }

    public static Order getOrder() {
        // int id, List<OrderItem > items, String time, double rating, String comment, boolean delivered, boolean anonComment, boolean archivedComment) {

//        Article article2 = new Article("Pancake", "Best in town", "150rsd", R.drawable.pancake);
//        Article article3 = new Article("Omelette", "Basic breakfast", "200rsd", R.drawable.omelette);
//
//        OrderItem item1 = new OrderItem(2, article2);
//        OrderItem item2 = new OrderItem(1, article3);
//
//        List<OrderItem> orderItems = new ArrayList<>();
//        orderItems.add(item1);
//        orderItems.add(item2);
//
//        Customer customer1 = new Customer(1, "Clark", "Adams", "clark123", "123456",
//                false, "907 Carriage Street Bay Shore, NY 11706");
//
        Order order = new Order(1, new ArrayList<>(), "30min", 5, "", false,
               false, false, new Customer());
//
        return order;
    }


    public static List<Order> getOrders() {
       // nt id, String name, String surname, String username, String password, boolean isBlocked, String address
//        Customer customer1 = new Customer(1, "Clark", "Adams", "clark123", "123456",
//                false, "907 Carriage Street Bay Shore, NY 11706");
//        Customer customer2 = new Customer(2, "Reily", "Smith", "reily123",
//                "6543211", false, "5 Lakewood Street Commack, NY 11725");
//
//
//        Article article1 = new Article("Burger", "Best of our own", "300rsd", R.drawable.food1);
//
//        OrderItem item1 = new OrderItem(2, article1);
//
//        List<OrderItem> orderItems1 = new ArrayList<>();
//        orderItems1.add(item1);
//
//
//        Order order1 = new Order(1, orderItems1, "30min", 5, "Best burger in town",
//                true, false, false, customer1);
//
//        Article article2 = new Article("Pancake", "Best in town", "150rsd", R.drawable.pancake);
//        Article article3 = new Article("Omelette", "Basic breakfast", "200rsd", R.drawable.omelette);
//
//        OrderItem item3 = new OrderItem(2, article2);
//        OrderItem item2 = new OrderItem(1, article3);
//        ArrayList<OrderItem> orderItems2 = new ArrayList<>();
//        orderItems2.add(item3);
//        orderItems2.add(item2);
//
//        Order order = new Order(2, orderItems2, "50min", 5, "It was great",
//                true, false, false, customer2);

        ArrayList<Order> orders = new ArrayList<>();
//        orders.add(order);
//        orders.add(order1);

        return orders;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static List<Discount> getDiscounts() {
//        Article article1 = new Article("Burger", "Best of our own", "300rsd", R.drawable.food1);
//        Article article2 = new Article("Pancake", "Best in town", "150rsd", R.drawable.pancake);
//        Article article3 = new Article("Omelette", "Basic breakfast", "200rsd", R.drawable.omelette);


        //Article article, int percentage, LocalDate from, LocalDate to
//        Discount discount1 = new Discount(article1, 20, LocalDate.now(),LocalDate.now().plusDays(2));
//        Discount discount2 = new Discount(article2, 10, LocalDate.now().plusDays(2),LocalDate.now().plusDays(4));

        List<Discount> discounts = new ArrayList<>();
//        discounts.add(discount1);
//        discounts.add(discount2);

        return discounts;
    }

    public static List<Customer> getCustomers() {
        Customer customer1 = new Customer(1, "Clark", "Adams", "clark123", "123456",
                false, "907 Carriage Street Bay Shore, NY 11706");
        Customer customer2 = new Customer(2, "Reily", "Smith", "reily123",
                "6543211", false, "5 Lakewood Street Commack, NY 11725");


        Customer customer3 = new Customer(3, "Darrin", "Cook", "darrin123", "123456",
                false, "907 Carriage Street Bay Shore, NY 11706");
        Customer customer4 = new Customer(4, "Dale", "Becker", "dale123",
                "6543999", true, "1938  McVaney Road, Forest City, NC");
        List<Customer> customers = new ArrayList<>();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);
        customers.add(customer4);

        return customers;
    }
}
