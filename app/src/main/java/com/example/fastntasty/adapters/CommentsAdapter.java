package com.example.fastntasty.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fastntasty.R;
import com.example.fastntasty.activities.CommentActivity;
import com.example.fastntasty.activities.MyArticlesActivity;
import com.example.fastntasty.activities.UpdateArticleActivity;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.OrderService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder>{
    private List<Order> mData;
    private Context context;

    static final String TAG = CommentsAdapter.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    public CommentsAdapter(List<Order> data, Context context) {
        this.mData = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView comment;
        TextView customer;
        TextView rating;
        Button archive;

        ViewHolder(View itemView) {
            super(itemView);

            comment = itemView.findViewById(R.id.comment);
            customer = itemView.findViewById(R.id.customer);
            rating = itemView.findViewById(R.id.rating);
            archive = itemView.findViewById(R.id.archive);
        }
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_list, parent, false);
        return new CommentsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder holder, int position) {
        Order order = mData.get(position);

        if(order.isAnonymous()) {
            holder.customer.setText("Anonymous user");
        }
        else if(!order.isAnonymous()){
            holder.customer.setText(order.getCustomer().getName());
        }
        holder.rating.setText("Rating: " + String.valueOf(order.getRating()));
        holder.comment.setText(order.getComment());;



        holder.archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //  Toast.makeText(view.getContext(), "Comment successfully archived!",Toast.LENGTH_LONG).show();
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }
                OrderService orderService = retrofit.create(OrderService.class);
                //ArticleService articleService = retrofit.create(ArticleService.class);
                Call<Order> call = orderService.setArchived((long) order.getId(), order);

                call.enqueue(new Callback<Order>() {
                    @Override
                    public void onResponse(Call<Order> call, Response<Order> response) {
                        // recyclerView.setAdapter(new MyArticlesAdapter(response.body(), MyArticlesActivity.this));
                        System.out.println("SUCCESS22");
                        System.out.println(response.body());
                        Toast.makeText(view.getContext(), "Successfully archived!",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, CommentActivity.class);
                        context.startActivity(intent);
                        // recyclerView.setAdapter(new MovieListAdapter(response.body().getMovies(), getApplicationContext()));
                    }
                    @Override
                    public void onFailure(Call<Order> call, Throwable throwable) {
                        Log.e(TAG, throwable.toString());
                        System.out.println("hereee");
                    }
                });

            }
        });


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
