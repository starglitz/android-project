package com.example.fastntasty.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fastntasty.R;
import com.example.fastntasty.activities.ArticleActivity;
import com.example.fastntasty.model.Article;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ViewHolder>{

    private List<Article> mData;
    private Context context;

    public ArticleListAdapter(List<Article> data, Context context) {
        this.mData = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;
        TextView price;
        ImageView path;
        Button add;

        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            description =  itemView.findViewById(R.id.description);
            price = itemView.findViewById(R.id.price);
            //overview = itemView.findViewById(R.id.tvOverview);
            path = itemView.findViewById(R.id.imageArticle);
            //imgView.setBackgroundResource(R.drawable.img1)
            add = itemView.findViewById(R.id.view_more);

        }
    }

    @Override
    public ArticleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.articles_list, parent, false);
        return new ArticleListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArticleListAdapter.ViewHolder holder, int position) {
        Article article = mData.get(position);

        File imgFile = new File(article.getPath());
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            holder.path.setImageBitmap(myBitmap);
//            ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
//
//            myImage.setImageBitmap(myBitmap);


        }
       // holder.id.setText(article.getId());
        holder.name.setText(article.getName());
        holder.price.setText(String.valueOf(article.getPrice()));
        holder.description.setText(article.getDescription());

        //String imageName =  article.getPath().replaceAll("/images/", "");
//        String imageName =  article.getPath();
//        InputStream bitmap=null;
//
//        try {
//            bitmap= context.getAssets().open(imageName);
//            Bitmap bit=BitmapFactory.decodeStream(bitmap);
//            holder.path.setImageBitmap(bit);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if(bitmap!=null) {
//                try {
//                    bitmap.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }

        byte[] bytes = Base64.decode(article.getPath(), Base64.DEFAULT);
        Bitmap bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        holder.path.setImageBitmap(bm);


        //File imageFile = new  File("../../res/drawable" + imageName);
       // Bitmap bmImg = BitmapFactory.decodeFile("app/res/drawable/pancake.jpg");
        //Bitmap bmImg = BitmapFactory.decodeFile("/sdcard/pancake.jpg");
        //holder.path.setImageBitmap(bmImg);
        //holder.discounts.setText(article.getDiscounts());
        //holder.seller.setText(article.getSeller().getSellerName());
//        holder.title.setText(movie.getTitle());
//        holder.releaseDate.setText(movie.getReleaseDate());
//        holder.vote.setText(movie.getVoteAverage().toString());
//        holder.overview.setText(movie.getOverview());
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("was clicked");
                Intent intent = new Intent(context, ArticleActivity.class);
                System.out.println(article);
                Bundle b = new Bundle();
                b.putString("name", article.getName());
                b.putString("price", String.valueOf(article.getPrice()));
                b.putString("description", article.getDescription());
                //b.putString("path", article.getPath());
                b.putLong("ID", article.getId());
                b.putLong("SellerID", article.getSeller().getId());
                intent.putExtras(b);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}
