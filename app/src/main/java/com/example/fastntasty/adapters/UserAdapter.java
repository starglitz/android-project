package com.example.fastntasty.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.fastntasty.R;
import com.example.fastntasty.tools.MockupData;

import com.example.fastntasty.model.Customer;

public class UserAdapter extends BaseAdapter {

    private Activity activity;

    public UserAdapter(Activity activity) {
        this.activity = activity;
    }

    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return MockupData.getCustomers().size();
    }

    /*
     * Ova metoda vraca pojedinacan element na osnovu pozicije
     * */
    @Override
    public Object getItem(int position) {
        return MockupData.getCustomers().get(position);
    }


    /*
     * Ova metoda vraca jedinstveni identifikator, za adaptere koji prikazuju
     * listu ili niz, pozicija je dovoljno dobra. Naravno mozemo iskoristiti i
     * jedinstveni identifikator objekta, ako on postoji.
     * */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * Ova metoda popunjava pojedinacan element ListView-a podacima.
     * Ako adapter cuva listu od n elemenata, adapter ce u petlji ici
     * onoliko puta koliko getCount() vrati. Prilikom svake iteracije
     * uzece java objekat sa odredjene poziciuje (model) koji cuva podatke,
     * i layout koji treba da prikaze te podatke (view) npr R.layout.cinema_list.
     * Kada adapter ima model i view, prosto ce uzeti podatke iz modela,
     * popuniti view podacima i poslati listview da prikaze, i nastavice
     * sledecu iteraciju.
     * */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       View vi = convertView;

        Customer customer = MockupData.getCustomers().get(position);

        if (convertView == null)
            vi = activity.getLayoutInflater().inflate(R.layout.users_list, null);

        TextView username = (TextView) vi.findViewById(R.id.user_username);
        TextView name = (TextView) vi.findViewById(R.id.user_name);
        //      TextView rating = (TextView) vi.findViewById(R.id.rating);

        username.setText(customer.getUsername());
        name.setText(customer.getName() + " " + customer.getSurname());

        Switch mySwitch = (Switch) vi.findViewById(R.id.is_user_blocked);


        mySwitch.setTag("TAG");

        if(!customer.isEnabled()) {
            mySwitch.setChecked(true);
        }
        else {
            mySwitch.setChecked(false);
            mySwitch.setText("blocked");
        }

        Context context = vi.getContext();
        View finalVi = vi;
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(isChecked) {
//                    Toast.makeText(parent.getContext(), "User unblocked", Toast.LENGTH_SHORT).show();
                        mySwitch.setText("active");
                    Log.d("test","unblocked");
                }
                if(!isChecked) {
                  //  Toast.makeText(parent.getContext(), "User blocked", Toast.LENGTH_SHORT).show();
                    mySwitch.setText("blocked");
                    Log.d("test","blocked");
                }
            }
        });


        return vi;
    }
}
