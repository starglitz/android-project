package com.example.fastntasty.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fastntasty.R;
import com.example.fastntasty.activities.ArticleActivity;
import com.example.fastntasty.activities.SellersArticlesActivity;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Seller;

import org.w3c.dom.Text;

import java.io.File;
import java.util.List;

public class SellersListAdapter extends RecyclerView.Adapter<SellersListAdapter.ViewHolder>{

    private List<Seller> mData;
    private Context context;


    public SellersListAdapter(List<Seller> data, Context context) {
        this.mData = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView address;
        TextView since;
        TextView rating;

        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            address =  itemView.findViewById(R.id.address);
            since = itemView.findViewById(R.id.since);
            rating = itemView.findViewById(R.id.rating);
            //overview = itemView.findViewById(R.id.tvOverview);
            //path = itemView.findViewById(R.id.imageArticle);
            //imgView.setBackgroundResource(R.drawable.img1)
        }
    }

    @Override
    public SellersListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sellers_list, parent, false);
        return new SellersListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SellersListAdapter.ViewHolder holder, int position) {
        //Article article = mData.get(position);
        Seller seller = mData.get(position);

//        File imgFile = new File(article.getPath());
//        if(imgFile.exists()){
//
//            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//            holder.path.setImageBitmap(myBitmap);
////            ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
////
////            myImage.setImageBitmap(myBitmap);
//        }
        // holder.id.setText(article.getId());
        holder.name.setText(seller.getSellerName());
        holder.address.setText(seller.getAddress());
        holder.since.setText(seller.getSince());
        if(seller.getRating() == 0) {
            holder.rating.setText("This seller hasn't been rated yet");
        }
        else {
            holder.rating.setText("Average rating: " + String.valueOf(seller.getRating()));
        }
//        holder.price.setText(article.getPrice());
//        holder.description.setText(article.getDescription());
        //holder.discounts.setText(article.getDiscounts());
        //holder.seller.setText(article.getSeller().getSellerName());
//        holder.title.setText(movie.getTitle());
//        holder.releaseDate.setText(movie.getReleaseDate());
//        holder.vote.setText(movie.getVoteAverage().toString());
//        holder.overview.setText(movie.getOverview());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,SellersArticlesActivity.class);
                Bundle b = new Bundle();
                b.putInt("SellerID", seller.getId());
                b.putFloat("SellerRating", seller.getRating());
                intent.putExtras(b);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
