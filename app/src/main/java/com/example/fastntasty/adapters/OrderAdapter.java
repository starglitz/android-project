package com.example.fastntasty.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fastntasty.R;
import com.example.fastntasty.tools.MockupData;

import java.util.List;

import com.example.fastntasty.model.Order;
import com.example.fastntasty.model.OrderItem;

public class OrderAdapter extends BaseAdapter {

    private Activity activity;

    public OrderAdapter(Activity activity) {
        this.activity = activity;
    }

    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return MockupData.getOrders().size();
    }

    /*
     * Ova metoda vraca pojedinacan element na osnovu pozicije
     * */
    @Override
    public Object getItem(int position) {
        return MockupData.getOrders().get(position);
    }


    /*
     * Ova metoda vraca jedinstveni identifikator, za adaptere koji prikazuju
     * listu ili niz, pozicija je dovoljno dobra. Naravno mozemo iskoristiti i
     * jedinstveni identifikator objekta, ako on postoji.
     * */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * Ova metoda popunjava pojedinacan element ListView-a podacima.
     * Ako adapter cuva listu od n elemenata, adapter ce u petlji ici
     * onoliko puta koliko getCount() vrati. Prilikom svake iteracije
     * uzece java objekat sa odredjene poziciuje (model) koji cuva podatke,
     * i layout koji treba da prikaze te podatke (view) npr R.layout.cinema_list.
     * Kada adapter ima model i view, prosto ce uzeti podatke iz modela,
     * popuniti view podacima i poslati listview da prikaze, i nastavice
     * sledecu iteraciju.
     * */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        Order order = MockupData.getOrders().get(position);

        List<OrderItem> orderItems = order.getItems();



        if (convertView == null)
            vi = activity.getLayoutInflater().inflate(R.layout.order_review, null);

        TextView id = (TextView) vi.findViewById(R.id.order_id);
        TextView time = (TextView) vi.findViewById(R.id.time);
        ListView lv = (ListView) vi.findViewById(R.id.order_items);
  //      TextView rating = (TextView) vi.findViewById(R.id.rating);

        id.setText(Integer.toString(order.getId()));
        time.setText(order.getTime());
        //android.R.layout.simple_list_item_2


        ArrayAdapter<OrderItem> adapter = new ArrayAdapter<OrderItem>(this.activity,R.layout.order_item_text, R.id.order_item_textview, orderItems);
        lv.setAdapter(adapter);

        ViewGroup.LayoutParams params = lv.getLayoutParams();
// Changes the height and width to the specified *pixels*
        params.height = (int) orderItems.size() * 80;
        lv.setLayoutParams(params);





        // comment.setText(order.getComment());
       // rating.setText(Double.toString(order.getRating()));

        return vi;
    }
}
