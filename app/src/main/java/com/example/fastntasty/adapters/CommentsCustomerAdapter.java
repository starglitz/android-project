package com.example.fastntasty.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fastntasty.R;
import com.example.fastntasty.model.Order;

import java.util.List;

import retrofit2.Retrofit;

public class CommentsCustomerAdapter extends RecyclerView.Adapter<CommentsCustomerAdapter.ViewHolder>{
    private List<Order> mData;
    private Context context;

    static final String TAG = CommentsCustomerAdapter.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    public CommentsCustomerAdapter(List<Order> data, Context context) {
        this.mData = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView comment;
        TextView customer;
        TextView rating;

        ViewHolder(View itemView) {
            super(itemView);

            comment = itemView.findViewById(R.id.comment);
            customer = itemView.findViewById(R.id.customer);
            rating = itemView.findViewById(R.id.rating);
        }
    }

    @Override
    public CommentsCustomerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_customer_list, parent, false);
        return new CommentsCustomerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentsCustomerAdapter.ViewHolder holder, int position) {
        Order order = mData.get(position);

        if(order.isAnonymous()) {
            holder.customer.setText("Anonymous user");
        }
        else if(!order.isAnonymous()){
            holder.customer.setText(order.getCustomer().getName());
        }
        holder.rating.setText("Rating: " + String.valueOf(order.getRating()));
        holder.comment.setText(order.getComment());;






    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}
