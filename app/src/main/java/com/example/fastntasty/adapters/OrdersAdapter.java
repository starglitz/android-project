package com.example.fastntasty.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fastntasty.R;
import com.example.fastntasty.activities.AddCommentActivity;
import com.example.fastntasty.activities.ArticleActivity;
import com.example.fastntasty.activities.UpdateArticleActivity;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.model.OrderItem;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder>{
    private List<Order> mData;
    private Context context;

    public OrdersAdapter(List<Order> data, Context context) {
        this.mData = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id;
        TextView time;
        ListView lv;
        Button rateOrder;

        ViewHolder(View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.order_id);
            time = itemView.findViewById(R.id.time);
            lv = itemView.findViewById(R.id.order_items);
            rateOrder = itemView.findViewById(R.id.rate_order_btn);
        }
    }

    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_review, parent, false);
        return new OrdersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrdersAdapter.ViewHolder holder, int position) {
        Order order = mData.get(position);

        holder.id.setText(String.valueOf(order.getId()));

        holder.time.setText(order.getTime().substring(0, Math.min(order.getTime().length(), 10)));

        ArrayAdapter<OrderItem> adapter = new ArrayAdapter<OrderItem>(context,R.layout.order_item_text, R.id.order_item_textview, order.getItems());
        holder.lv.setAdapter(adapter);

        ViewGroup.LayoutParams params = holder.lv.getLayoutParams();
// Changes the height and width to the specified *pixels*
        params.height = (int) order.getItems().size() * 100;
        holder.lv.setLayoutParams(params);

        holder.rateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddCommentActivity.class);
                Bundle b = new Bundle();
                b.putLong("ID", order.getId());
                intent.putExtras(b);
                context.startActivity(intent);
                // Toast.makeText(view.getContext(), "Article name: " + article.getName(),Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
