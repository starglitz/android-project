package com.example.fastntasty.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fastntasty.R;
import com.example.fastntasty.activities.MyArticlesActivity;
import com.example.fastntasty.activities.UpdateArticleActivity;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.User;
import com.example.fastntasty.model.UserBlock;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.UserService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder>{
    private List<UserBlock> mData;
    private Context context;

    static final String TAG = UsersAdapter.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    public UsersAdapter(List<UserBlock> data, Context context) {
        this.mData = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView username;
        Switch isBlocked;

        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.user_name);
            username = itemView.findViewById(R.id.user_username);
            isBlocked = itemView.findViewById(R.id.is_user_blocked);

        }
    }


    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_list, parent, false);
        return new UsersAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UsersAdapter.ViewHolder holder, int position) {
        UserBlock user = mData.get(position);

        holder.name.setText(user.getName());
        holder.username.setText(user.getUsername());


        holder.isBlocked.setTag("TAG");

        if(user.isEnabled()) {
            System.out.println("SOMETHING HAPPENED");
            holder.isBlocked.setChecked(true);
        }
        else {
            holder.isBlocked.setChecked(false);
            holder.isBlocked.setText("blocked");
        }

//        Context context = vi.getContext();
//        View finalVi = vi;
        holder.isBlocked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    holder.isBlocked.setText("active");
                    Log.d("test","unblocked");

                    user.setEnabled(true);

                    if (retrofit == null) {
                        retrofit = new Retrofit.Builder()
                                .baseUrl(BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                    }
                    UserService userService = retrofit.create(UserService.class);
                   // Call<Article> call = articleService.deleteArticle(article.getId());
                    Call<UserBlock> call = userService.block(user.getId(), new UserBlock(user.getId(), user.isEnabled()));

                    call.enqueue(new Callback<UserBlock>() {
                        @Override
                        public void onResponse(Call<UserBlock> call, Response<UserBlock> response) {
                            System.out.println("SUCCESS");
                            //System.out.println(response.body());

                        }
                        @Override
                        public void onFailure(Call<UserBlock> call, Throwable throwable) {
                            Log.e(TAG, throwable.toString());
                            System.out.println("hereee");
                        }
                    });


                }
                if(!isChecked) {
                    holder.isBlocked.setText("blocked");
                    Log.d("test","blocked");
                    user.setEnabled(false);

                    if (retrofit == null) {
                        retrofit = new Retrofit.Builder()
                                .baseUrl(BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                    }
                    UserService userService = retrofit.create(UserService.class);
                    // Call<Article> call = articleService.deleteArticle(article.getId());
                    Call<UserBlock> call = userService.block(user.getId(), new UserBlock(user.getId(), user.isEnabled()));

                    call.enqueue(new Callback<UserBlock>() {
                        @Override
                        public void onResponse(Call<UserBlock> call, Response<UserBlock> response) {
                            System.out.println("SUCCESS");
                            //System.out.println(response.body());

                        }
                        @Override
                        public void onFailure(Call<UserBlock> call, Throwable throwable) {
                            Log.e(TAG, throwable.toString());
                            System.out.println("hereee");
                        }
                    });
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}
