package com.example.fastntasty.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fastntasty.R;
import com.example.fastntasty.activities.ArticleActivity;
import com.example.fastntasty.activities.DiscountActivity;
import com.example.fastntasty.activities.MyArticlesActivity;
import com.example.fastntasty.activities.NewArticleActivity;
import com.example.fastntasty.activities.UpdateArticleActivity;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.services.ArticleService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyArticlesAdapter  extends RecyclerView.Adapter<MyArticlesAdapter.ViewHolder>{

    private List<Article> mData;
    private Context context;

    static final String TAG = MyArticlesAdapter.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    public MyArticlesAdapter(List<Article> data, Context context) {
        this.mData = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;
        TextView price;
        ImageView path;

        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            description =  itemView.findViewById(R.id.description);
            price = itemView.findViewById(R.id.price);
            //overview = itemView.findViewById(R.id.tvOverview);
            path = itemView.findViewById(R.id.imageArticle);
            //imgView.setBackgroundResource(R.drawable.img1)
            edit = itemView.findViewById(R.id.edit);
            delete = itemView.findViewById(R.id.delete);
        }
    }

    @Override
    public MyArticlesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_articles_list, parent, false);
        return new MyArticlesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyArticlesAdapter.ViewHolder holder, int position) {
        Article article = mData.get(position);

//        File imgFile = new File(article.getPath());
//        if(imgFile.exists()){
//
//            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//            holder.path.setImageBitmap(myBitmap);
////            ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
////
////            myImage.setImageBitmap(myBitmap);
//
//
//        }


        // holder.id.setText(article.getId());
        holder.name.setText(article.getName());
        holder.price.setText(String.valueOf(article.getPrice()));
        holder.description.setText(article.getDescription());



        byte[] bytes = Base64.decode(article.getPath(), Base64.DEFAULT);
        Bitmap bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        holder.path.setImageBitmap(bm);



        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdateArticleActivity.class);
                Bundle b = new Bundle();
                b.putString("ID", article.getId().toString());
                //b.putLong("ID", article.getId());
                b.putString("name", article.getName());
                b.putString("description", article.getDescription());
                b.putString("price", String.valueOf(article.getPrice()));
                //b.putString("path", article.getPath());
                intent.putExtras(b);
                context.startActivity(intent);
               // Toast.makeText(view.getContext(), "Article name: " + article.getName(),Toast.LENGTH_LONG).show();
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (retrofit == null) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }
                ArticleService articleService = retrofit.create(ArticleService.class);
                Call<Article> call = articleService.deleteArticle(article.getId());

                call.enqueue(new Callback<Article>() {
                    @Override
                    public void onResponse(Call<Article> call, Response<Article> response) {
                       // recyclerView.setAdapter(new MyArticlesAdapter(response.body(), MyArticlesActivity.this));
                        System.out.println("SUCCESS");
                        System.out.println(response.body());
                        Toast.makeText(view.getContext(), "Successfully deleted article",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, MyArticlesActivity.class);
                        context.startActivity(intent);
                        // recyclerView.setAdapter(new MovieListAdapter(response.body().getMovies(), getApplicationContext()));
                    }
                    @Override
                    public void onFailure(Call<Article> call, Throwable throwable) {
                        Log.e(TAG, throwable.toString());
                        System.out.println("hereee");
                    }
                });

            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}
