package com.example.fastntasty.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.fastntasty.R;
import com.example.fastntasty.tools.MockupData;

import com.example.fastntasty.model.Seller;

public class SellersAdapter extends BaseAdapter {
    private Activity activity;

    public SellersAdapter(Activity activity) {
        this.activity = activity;
    }

    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return MockupData.getSellers().size();
    }

    /*
     * Ova metoda vraca pojedinacan element na osnovu pozicije
     * */
    @Override
    public Object getItem(int position) {
        return MockupData.getSellers().get(position);
    }


    /*
     * Ova metoda vraca jedinstveni identifikator, za adaptere koji prikazuju
     * listu ili niz, pozicija je dovoljno dobra. Naravno mozemo iskoristiti i
     * jedinstveni identifikator objekta, ako on postoji.
     * */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * Ova metoda popunjava pojedinacan element ListView-a podacima.
     * Ako adapter cuva listu od n elemenata, adapter ce u petlji ici
     * onoliko puta koliko getCount() vrati. Prilikom svake iteracije
     * uzece java objekat sa odredjene poziciuje (model) koji cuva podatke,
     * i layout koji treba da prikaze te podatke (view) npr R.layout.cinema_list.
     * Kada adapter ima model i view, prosto ce uzeti podatke iz modela,
     * popuniti view podacima i poslati listview da prikaze, i nastavice
     * sledecu iteraciju.
     * */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        Seller seller = MockupData.getSellers().get(position);

        if (convertView == null)
            vi = activity.getLayoutInflater().inflate(R.layout.sellers_list, null);

        TextView name = (TextView) vi.findViewById(R.id.name);
        TextView address = (TextView) vi.findViewById(R.id.address);
        TextView since = (TextView) vi.findViewById(R.id.since);
//        ImageView image = (ImageView) vi.findViewById(R.id.item_icon);

        name.setText(seller.getName());
        address.setText(seller.getAddress());
        since.setText(seller.getSince());

//        if (cinema.getAvatar() != -1){
//            image.setImageResource(cinema.getAvatar());
//        }

        return vi;
    }
    }
