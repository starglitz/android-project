package com.example.fastntasty.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.fastntasty.R;
import com.example.fastntasty.activities.ArticleActivity;
import com.example.fastntasty.activities.MyArticlesActivity;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.Order;
import com.example.fastntasty.model.OrderItem;
import com.example.fastntasty.services.ArticleService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.ViewHolder>{
    private List<OrderItem> mData;
    private Context context;

    static final String TAG = OrderItemAdapter.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";
    static Retrofit retrofit = null;

    public OrderItemAdapter(List<OrderItem> data, Context context) {
        this.mData = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView amount;
        TextView price;


        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.item_name2);
            amount = itemView.findViewById(R.id.item_amount2);
            price = itemView.findViewById(R.id.item_price2);

        }
    }

    @Override
    public OrderItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_items, parent, false);
        return new OrderItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderItemAdapter.ViewHolder holder, int position) {
        OrderItem item = mData.get(position);
        //Article article = mData.get(position);

        holder.amount.setText("Amount: " + String.valueOf(item.getAmount()));
        holder.name.setText( "Article name: " + String.valueOf(item.getArticle().getName()));
        holder.price.setText("Price of this order item: " + String.valueOf(item.getAmount() * item.getArticle().getPrice()));


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



}
