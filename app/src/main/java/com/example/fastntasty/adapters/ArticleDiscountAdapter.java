package com.example.fastntasty.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.fastntasty.R;
import com.example.fastntasty.tools.MockupData;

import com.example.fastntasty.model.Article;

public class ArticleDiscountAdapter extends BaseAdapter {
    private Activity activity;

    public ArticleDiscountAdapter(Activity activity) {
        this.activity = activity;
    }

    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @Override
    public int getCount() {
        return MockupData.getArticles().size();
    }

    /*
     * Ova metoda vraca pojedinacan element na osnovu pozicije
     * */
    @Override
    public Object getItem(int position) {
        return MockupData.getArticles().get(position);
    }


    /*
     * Ova metoda vraca jedinstveni identifikator, za adaptere koji prikazuju
     * listu ili niz, pozicija je dovoljno dobra. Naravno mozemo iskoristiti i
     * jedinstveni identifikator objekta, ako on postoji.
     * */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * Ova metoda popunjava pojedinacan element ListView-a podacima.
     * Ako adapter cuva listu od n elemenata, adapter ce u petlji ici
     * onoliko puta koliko getCount() vrati. Prilikom svake iteracije
     * uzece java objekat sa odredjene poziciuje (model) koji cuva podatke,
     * i layout koji treba da prikaze te podatke (view) npr R.layout.cinema_list.
     * Kada adapter ima model i view, prosto ce uzeti podatke iz modela,
     * popuniti view podacima i poslati listview da prikaze, i nastavice
     * sledecu iteraciju.
     * */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        Article article = MockupData.getArticles().get(position);


        if (convertView == null)
            vi = activity.getLayoutInflater().inflate(R.layout.articles_discount_list, null);

        TextView name = (TextView) vi.findViewById(R.id.name);
//        TextView description = (TextView) vi.findViewById(R.id.description);
        TextView price = (TextView) vi.findViewById(R.id.price);
//        ImageView img = (ImageView) vi.findViewById(R.id.imageArticle);
//        TextView amount = (TextView) vi.findViewById(R.id.amountOfItems);

        name.setText(article.getName());
//        description.setText(item.getArticle().getDescription());
        price.setText(article.getPrice());
//        amount.setText(Integer.toString(item.getAmount()));

//        img.setTag(item.getArticle().getImgPath());
//
//        img.setImageResource(item.getArticle().getImgPath());






//        if (cinema.getAvatar() != -1){
//            image.setImageResource(cinema.getAvatar());
//        }

        return vi;
    }
}
