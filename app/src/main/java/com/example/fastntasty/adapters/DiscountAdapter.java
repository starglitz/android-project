package com.example.fastntasty.adapters;

import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.example.fastntasty.R;
import com.example.fastntasty.tools.MockupData;

import com.example.fastntasty.model.Discount;

public class DiscountAdapter extends BaseAdapter {
    private Activity activity;

    public DiscountAdapter(Activity activity) {
        this.activity = activity;
    }

    /*
     * Ova metoda vraca ukupan broj elemenata u listi koje treba prikazati
     * */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int getCount() {
        return MockupData.getDiscounts().size();
    }

    /*
     * Ova metoda vraca pojedinacan element na osnovu pozicije
     * */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public Object getItem(int position) {
        return MockupData.getDiscounts().get(position);
    }


    /*
     * Ova metoda vraca jedinstveni identifikator, za adaptere koji prikazuju
     * listu ili niz, pozicija je dovoljno dobra. Naravno mozemo iskoristiti i
     * jedinstveni identifikator objekta, ako on postoji.
     * */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * Ova metoda popunjava pojedinacan element ListView-a podacima.
     * Ako adapter cuva listu od n elemenata, adapter ce u petlji ici
     * onoliko puta koliko getCount() vrati. Prilikom svake iteracije
     * uzece java objekat sa odredjene poziciuje (model) koji cuva podatke,
     * i layout koji treba da prikaze te podatke (view) npr R.layout.cinema_list.
     * Kada adapter ima model i view, prosto ce uzeti podatke iz modela,
     * popuniti view podacima i poslati listview da prikaze, i nastavice
     * sledecu iteraciju.
     * */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        Discount discount = MockupData.getDiscounts().get(position);


        if (convertView == null)
            vi = activity.getLayoutInflater().inflate(R.layout.discounts_list, null);

        TextView articleName = (TextView) vi.findViewById(R.id.article);
        TextView percentage = (TextView) vi.findViewById(R.id.discount);
        TextView from = (TextView) vi.findViewById(R.id.from);
        TextView to = (TextView) vi.findViewById(R.id.to);

       // articleName.setText(discount.getArticles();
        percentage.setText(Integer.toString(discount.getPercent()));
        from.setText(discount.getDateFrom().toString());
        to.setText(discount.getDateTo().toString());
//
//        customer.setText(order.getCustomer().getName() + " " + order.getCustomer().getSurname());
//        comment.setText(order.getComment());
//        rating.setText(Double.toString(order.getRating()));

        return vi;
    }
}
