package com.example.fastntasty;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fastntasty.activities.MyArticlesActivity;
import com.example.fastntasty.activities.RegisterActivity;
import com.example.fastntasty.activities.RegisterSellerActivity;
import com.example.fastntasty.activities.SellersActivity;
import com.example.fastntasty.activities.UsersActivity;
import com.example.fastntasty.adapters.MyArticlesAdapter;
import com.example.fastntasty.model.Article;
import com.example.fastntasty.model.User;
import com.example.fastntasty.services.ArticleService;
import com.example.fastntasty.services.UserService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    static Retrofit retrofit = null;
    static final String TAG = MainActivity.class.getSimpleName();
    static final String BASE_URL = "http://192.168.43.48:8080/";

    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.clear().commit();


        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String userPref = sharedPreferences.getString("username", "");


        if(userPref != "") {
            System.out.println("USER PREF NOW: " + userPref);
            findLoggedIn();
        }


        Button registerCustomer = findViewById(R.id.activity_main_RegisterButton_Customer);
        registerCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegister();
            }
        });


        Button registerSeller = findViewById(R.id.activity_main_RegisterButton_Seller);
        registerSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegisterSeller();
            }
        });





            Button login = findViewById(R.id.activity_main_loginButton);
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EditText username = findViewById(R.id.activity_main_usernameEditText);
                    EditText password = findViewById(R.id.activity_main_passwordEditText);

                    String usernameVal = username.getText().toString();
                    String passwordVal = password.getText().toString();

                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    if(usernameVal == "" || passwordVal == "") {
                        Toast.makeText(MainActivity.this, "Make sure you fill both fields!" , Toast.LENGTH_LONG).show();
                        editor.clear().commit();
                    }
                    else {
                        editor.putString("username", usernameVal);
                        editor.commit();
                    }

                    login(usernameVal, passwordVal);
                }
            });

    }


    public void openRegister(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void openRegisterSeller() {
        Intent intent = new Intent(this, RegisterSellerActivity.class);
        startActivity(intent);
    }

    public void openHome() {
        Intent intent = new Intent(this, SellersActivity.class);
        startActivity(intent);
    }


    public void login(String username, String password){

        User user = new User(username, password);
        System.out.println("username: " + username);
        System.out.println("password: " + password);
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        userService = retrofit.create(UserService.class);

        Call<User> call = userService.login(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                System.out.println("USER:");
                System.out.println(user);
                System.out.println("STATUS CODE:");
                System.out.println(response.code());
                //Toast.makeText(MainActivity.this, (CharSequence) response.body(), Toast.LENGTH_LONG).show();
                if(response.code() == 404) {
                    Toast.makeText(MainActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
                }
                else if(response.code() == 403) {
                    Toast.makeText(MainActivity.this, "You are blocked from using this application!", Toast.LENGTH_SHORT).show();
                }

                else {if(username.equals(user.getUsername()) && password.equals(user.getPassword())){

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("username", user.getUsername());
                    editor.putString("role", String.valueOf(user.getRole()));
                    //editor.putString("id", String.valueOf(user.getId()));
                    editor.putLong("id",user.getId());
                    editor.commit();

                    System.out.println("ROLE: ");
                    System.out.println(user.getRole());

                    if(String.valueOf(user.getRole()).equals("CUSTOMER")) {
                        Intent intent = new Intent(MainActivity.this, SellersActivity.class);
                        startActivity(intent);
                    }
                    else if(String.valueOf(user.getRole()).equals("SELLER")) {
                        Intent intent = new Intent(MainActivity.this, MyArticlesActivity.class);
                        startActivity(intent);
                    }
                    else if(String.valueOf(user.getRole()).equals("ADMIN")) {
                        Intent intent = new Intent(MainActivity.this, UsersActivity.class);
                        startActivity(intent);
                    }
                } }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "Error on server side", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void findLoggedIn() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

        String userPref = sharedPreferences.getString("username", "");
        Long id = sharedPreferences.getLong("id", 0L);

        UserService userService = retrofit.create(UserService.class);

        Call<User> call = userService.findByUsername(userPref);
        //Call<List<Seller>> call = sellerService.getSellers();
        System.out.println("!!!");
        System.out.println(call);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                System.out.println(response.body());
                if(String.valueOf(user.getRole()).equals("CUSTOMER")) {
                    Intent intent = new Intent(MainActivity.this, SellersActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if(String.valueOf(user.getRole()).equals("SELLER")) {
                    Intent intent = new Intent(MainActivity.this, MyArticlesActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if(String.valueOf(user.getRole()).equals("ADMIN")) {
                    Intent intent = new Intent(MainActivity.this, UsersActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
                System.out.println("hereee");
            }
        });
    }
}